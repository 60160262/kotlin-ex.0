data class Person4(var name: String, var age: Int, var about: String) {
    constructor() : this("", 0, "")
}

fun writeCreationLog(p: Person4) {
    println("A new person ${p.name} was created.")
}

fun main() {
    val jake = Person4("Jake", 30, "Android developer")
        .also {
            writeCreationLog(it)
        }
}