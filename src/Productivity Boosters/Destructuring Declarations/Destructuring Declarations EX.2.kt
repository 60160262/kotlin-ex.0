data class User2(val username: String, val email: String)

fun getUser() = User2("Mary", "mary@somewhere.com")

fun main() {
    val user = getUser()
    val (username, email) = user
    println(username == user.component1())

    val (_, emailAddress) = getUser()

}